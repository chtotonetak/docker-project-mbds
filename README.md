### Docker Project

Project based on [MEAN Stack CRUD](https://github.com/CodAffection/MEAN-Stack-CRUD-Operations).

For building `docker-project-angular` image use `./src/AngularApp/Dockerfile`.

For building `docker-project-nodejs` image use `./src/NodeJS/Dockerfile`.

```bash
cd src/AngularApp
docker build -t docker-project-angular .
cd ../NodeJS
docker build -t docker-project-nodejs .
cd ../../
docker-compose up -d
```

Backups of images stored at files `docker-project-angular.tar` and `docker-project-nodejs.tar`.
Use `docker load` command for import.